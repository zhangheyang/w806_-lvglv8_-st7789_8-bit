##
## Auto Generated makefile by CDK
## Do not modify this file, and any manual changes will be erased!!!   
##
## BuildSet
ProjectName            :=W806_SDK
ConfigurationName      :=BuildSet
WorkspacePath          :=../
ProjectPath            :=./
IntermediateDirectory  :=Obj
OutDir                 :=$(IntermediateDirectory)
User                   :=36380
Date                   :=11/11/2021
CDKPath                :=D:/develop/Hardware/CDK
LinkerName             :=csky-elfabiv2-gcc
LinkerNameoption       :=
SIZE                   :=csky-elfabiv2-size
READELF                :=csky-elfabiv2-readelf
CHECKSUM               :=crc32
SharedObjectLinkerName :=
ObjectSuffix           :=.o
DependSuffix           :=.d
PreprocessSuffix       :=.i
DisassemSuffix         :=.asm
IHexSuffix             :=.ihex
BinSuffix              :=.bin
ExeSuffix              :=.elf
LibSuffix              :=.a
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
ElfInfoSwitch          :=-hlS
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
UnPreprocessorSwitch   :=-U
SourceSwitch           :=-c 
ObjdumpSwitch          :=-S
ObjcopySwitch          :=-O ihex
ObjcopyBinSwitch       :=-O binary
OutputFile             :=W806
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=W806_SDK.txt
MakeDirCommand         :=mkdir
LinkOptions            := -mcpu=ck804ef  -mhard-float  -nostartfiles -Wl,--gc-sections -T"$(ProjectPath)/gcc_csky.ld"
LinkOtherFlagsOption   :=-Wl,-zmax-page-size=1024 -mhard-float
IncludePackagePath     :=
IncludeCPath           := $(IncludeSwitch)D:/develop/Hardware/CDK/CSKY/MinGW/csky-abiv2-elf-toolchain/csky-elfabiv2/include $(IncludeSwitch). $(IncludeSwitch)../Include $(IncludeSwitch)../Include/arch/xt804 $(IncludeSwitch)../Include/arch/xt804/csi_core $(IncludeSwitch)../Include/arch/xt804/csi_dsp $(IncludeSwitch)../Include/driver $(IncludeSwitch)../Libraries/component/FreeRTOS/include $(IncludeSwitch)../Libraries/component/FreeRTOS/portable/xt804 $(IncludeSwitch)../lgvlV8 $(IncludeSwitch)../lgvlV8/port $(IncludeSwitch)../lgvlV8/src $(IncludeSwitch)../lgvlV8/src/core $(IncludeSwitch)../lgvlV8/src/draw $(IncludeSwitch)../lgvlV8/src/extra $(IncludeSwitch)../lgvlV8/src/extra/layouts $(IncludeSwitch)../lgvlV8/src/extra/layouts/flex $(IncludeSwitch)../lgvlV8/src/extra/layouts/grid $(IncludeSwitch)../lgvlV8/src/extra/themes $(IncludeSwitch)../lgvlV8/src/extra/themes/basic $(IncludeSwitch)../lgvlV8/src/extra/themes/default $(IncludeSwitch)../lgvlV8/src/extra/themes/mono $(IncludeSwitch)../lgvlV8/src/extra/widgets $(IncludeSwitch)../lgvlV8/src/extra/widgets/animimg $(IncludeSwitch)../lgvlV8/src/extra/widgets/calendar $(IncludeSwitch)../lgvlV8/src/extra/widgets/chart $(IncludeSwitch)../lgvlV8/src/extra/widgets/colorwheel $(IncludeSwitch)../lgvlV8/src/extra/widgets/imgbtn $(IncludeSwitch)../lgvlV8/src/extra/widgets/keyboard $(IncludeSwitch)../lgvlV8/src/extra/widgets/led $(IncludeSwitch)../lgvlV8/src/extra/widgets/list $(IncludeSwitch)../lgvlV8/src/extra/widgets/meter $(IncludeSwitch)../lgvlV8/src/extra/widgets/msgbox $(IncludeSwitch)../lgvlV8/src/extra/widgets/span $(IncludeSwitch)../lgvlV8/src/extra/widgets/spinbox $(IncludeSwitch)../lgvlV8/src/extra/widgets/spinner $(IncludeSwitch)../lgvlV8/src/extra/widgets/tabview $(IncludeSwitch)../lgvlV8/src/extra/widgets/tileview $(IncludeSwitch)../lgvlV8/src/extra/widgets/win $(IncludeSwitch)../lgvlV8/src/font $(IncludeSwitch)../lgvlV8/src/gpu $(IncludeSwitch)../lgvlV8/src/hal $(IncludeSwitch)../lgvlV8/src/misc $(IncludeSwitch)../lgvlV8/src/widgets $(IncludeSwitch)../lgvlV8/user $(IncludeSwitch)inc  
IncludeAPath           := $(IncludeSwitch)D:/develop/Hardware/CDK/CSKY/csi/csi_core/csi_cdk/ $(IncludeSwitch)D:/develop/Hardware/CDK/CSKY/csi/csi_core/include/ $(IncludeSwitch)D:/develop/Hardware/CDK/CSKY/csi/csi_driver/include/ $(IncludeSwitch). $(IncludeSwitch)../Include $(IncludeSwitch)../Include/arch/xt804 $(IncludeSwitch)../Include/arch/xt804/csi_core $(IncludeSwitch)../Include/arch/xt804/csi_dsp $(IncludeSwitch)../Libraries/component/FreeRTOS/include $(IncludeSwitch)../lgvlV8 $(IncludeSwitch)../lgvlV8/port $(IncludeSwitch)../lgvlV8/src $(IncludeSwitch)../lgvlV8/src/core $(IncludeSwitch)../lgvlV8/src/draw $(IncludeSwitch)../lgvlV8/src/extra $(IncludeSwitch)../lgvlV8/src/font $(IncludeSwitch)../lgvlV8/src/gpu $(IncludeSwitch)../lgvlV8/src/hal $(IncludeSwitch)../lgvlV8/src/misc $(IncludeSwitch)../lgvlV8/src/widgets $(IncludeSwitch)../lgvlV8/user $(IncludeSwitch)../lgvlV8/user/src/lv_demo_widgets  
Libs                   := -Wl,--start-group  -Wl,--end-group $(LibrarySwitch)dsp $(LibrarySwitch)m  
ArLibs                 := "dsp m" 
PackagesLibPath        :=
LibPath                :=$(LibraryPathSwitch).  $(PackagesLibPath) 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       :=csky-elfabiv2-ar rcu
CXX      :=csky-elfabiv2-g++
CC       :=csky-elfabiv2-gcc
AS       :=csky-elfabiv2-gcc
OBJDUMP  :=csky-elfabiv2-objdump
OBJCOPY  :=csky-elfabiv2-objcopy
CXXFLAGS := -mcpu=ck804ef  -mhard-float   $(PreprocessorSwitch)GCC_COMPILE=1  $(PreprocessorSwitch)TLS_CONFIG_CPU_XT804=1   -O2  -g  -Wall  -ffunction-sections -fdata-sections -c 
CFLAGS   := -mcpu=ck804ef  -mhard-float   $(PreprocessorSwitch)GCC_COMPILE=1  $(PreprocessorSwitch)TLS_CONFIG_CPU_XT804=1   -O2  -g  -Wall  -ffunction-sections -fdata-sections -c 
ASFLAGS  := -mcpu=ck804ef  -mhard-float    -Wa,--gdwarf2    


Objects0=$(IntermediateDirectory)/src_main$(ObjectSuffix) $(IntermediateDirectory)/src_wm_hal_msp$(ObjectSuffix) $(IntermediateDirectory)/src_wm_it$(ObjectSuffix) $(IntermediateDirectory)/src_lcd$(ObjectSuffix) $(IntermediateDirectory)/src_st7789_parallel$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_adc$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_cpu$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_gpio$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_hal$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_i2c$(ObjectSuffix) \
	$(IntermediateDirectory)/drivers_wm_internal_flash$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_pmu$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_pwm$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_spi$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_spi_flash$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_tim$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_touch$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_uart$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_wdg$(ObjectSuffix) $(IntermediateDirectory)/drivers_wm_lcd$(ObjectSuffix) \
	$(IntermediateDirectory)/port_lv_port_disp$(ObjectSuffix) $(IntermediateDirectory)/core_lv_disp$(ObjectSuffix) $(IntermediateDirectory)/core_lv_event$(ObjectSuffix) $(IntermediateDirectory)/core_lv_group$(ObjectSuffix) $(IntermediateDirectory)/core_lv_indev$(ObjectSuffix) $(IntermediateDirectory)/core_lv_indev_scroll$(ObjectSuffix) $(IntermediateDirectory)/core_lv_obj$(ObjectSuffix) $(IntermediateDirectory)/core_lv_obj_class$(ObjectSuffix) $(IntermediateDirectory)/core_lv_obj_draw$(ObjectSuffix) $(IntermediateDirectory)/core_lv_obj_pos$(ObjectSuffix) \
	$(IntermediateDirectory)/core_lv_obj_scroll$(ObjectSuffix) $(IntermediateDirectory)/core_lv_obj_style$(ObjectSuffix) $(IntermediateDirectory)/core_lv_obj_style_gen$(ObjectSuffix) $(IntermediateDirectory)/core_lv_obj_tree$(ObjectSuffix) $(IntermediateDirectory)/core_lv_refr$(ObjectSuffix) $(IntermediateDirectory)/core_lv_theme$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_draw_arc$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_draw_blend$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_draw_img$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_draw_label$(ObjectSuffix) \
	$(IntermediateDirectory)/draw_lv_draw_line$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_draw_mask$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_draw_rect$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_draw_triangle$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_img_buf$(ObjectSuffix) 

Objects1=$(IntermediateDirectory)/draw_lv_img_cache$(ObjectSuffix) $(IntermediateDirectory)/draw_lv_img_decoder$(ObjectSuffix) $(IntermediateDirectory)/extra_lv_extra$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_dejavu_16_persian_hebrew$(ObjectSuffix) \
	$(IntermediateDirectory)/font_lv_font_fmt_txt$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_loader$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_10$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_12$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_12_subpx$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_14$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_16$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_18$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_20$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_22$(ObjectSuffix) \
	$(IntermediateDirectory)/font_lv_font_montserrat_24$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_26$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_28$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_28_compressed$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_30$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_32$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_34$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_36$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_38$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_40$(ObjectSuffix) \
	$(IntermediateDirectory)/font_lv_font_montserrat_42$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_44$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_46$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_48$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_montserrat_8$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_simsun_16_cjk$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_unscii_16$(ObjectSuffix) $(IntermediateDirectory)/font_lv_font_unscii_8$(ObjectSuffix) $(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp$(ObjectSuffix) $(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp_osa$(ObjectSuffix) \
	$(IntermediateDirectory)/gpu_lv_gpu_nxp_vglite$(ObjectSuffix) $(IntermediateDirectory)/gpu_lv_gpu_stm32_dma2d$(ObjectSuffix) $(IntermediateDirectory)/hal_lv_hal_disp$(ObjectSuffix) $(IntermediateDirectory)/hal_lv_hal_indev$(ObjectSuffix) $(IntermediateDirectory)/hal_lv_hal_tick$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_anim$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_area$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_async$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_bidi$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_color$(ObjectSuffix) \
	$(IntermediateDirectory)/misc_lv_fs$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_gc$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_ll$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_log$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_math$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_mem$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_printf$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_style$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_style_gen$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_templ$(ObjectSuffix) \
	$(IntermediateDirectory)/misc_lv_timer$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_tlsf$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_txt$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_txt_ap$(ObjectSuffix) $(IntermediateDirectory)/misc_lv_utils$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_arc$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_bar$(ObjectSuffix) 

Objects2=$(IntermediateDirectory)/widgets_lv_btn$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_btnmatrix$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_canvas$(ObjectSuffix) \
	$(IntermediateDirectory)/widgets_lv_checkbox$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_dropdown$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_img$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_label$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_line$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_objx_templ$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_roller$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_slider$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_switch$(ObjectSuffix) $(IntermediateDirectory)/widgets_lv_table$(ObjectSuffix) \
	$(IntermediateDirectory)/widgets_lv_textarea$(ObjectSuffix) $(IntermediateDirectory)/bsp_board_init$(ObjectSuffix) $(IntermediateDirectory)/bsp_startup$(ObjectSuffix) $(IntermediateDirectory)/bsp_system$(ObjectSuffix) $(IntermediateDirectory)/bsp_trap_c$(ObjectSuffix) $(IntermediateDirectory)/bsp_vectors$(ObjectSuffix) $(IntermediateDirectory)/libc_libc_port$(ObjectSuffix) $(IntermediateDirectory)/FreeRTOS_croutine$(ObjectSuffix) $(IntermediateDirectory)/FreeRTOS_event_groups$(ObjectSuffix) $(IntermediateDirectory)/FreeRTOS_list$(ObjectSuffix) \
	$(IntermediateDirectory)/FreeRTOS_queue$(ObjectSuffix) $(IntermediateDirectory)/FreeRTOS_stream_buffer$(ObjectSuffix) $(IntermediateDirectory)/FreeRTOS_tasks$(ObjectSuffix) $(IntermediateDirectory)/FreeRTOS_timers$(ObjectSuffix) $(IntermediateDirectory)/xt804_cpu_task_sw$(ObjectSuffix) $(IntermediateDirectory)/xt804_port$(ObjectSuffix) $(IntermediateDirectory)/MemMang_heap_5$(ObjectSuffix) $(IntermediateDirectory)/lv_demo_widgets_lv_demo_widgets$(ObjectSuffix) $(IntermediateDirectory)/flex_lv_flex$(ObjectSuffix) $(IntermediateDirectory)/grid_lv_grid$(ObjectSuffix) \
	$(IntermediateDirectory)/basic_lv_theme_basic$(ObjectSuffix) $(IntermediateDirectory)/default_lv_theme_default$(ObjectSuffix) $(IntermediateDirectory)/mono_lv_theme_mono$(ObjectSuffix) $(IntermediateDirectory)/animimg_lv_animimg$(ObjectSuffix) $(IntermediateDirectory)/calendar_lv_calendar$(ObjectSuffix) $(IntermediateDirectory)/calendar_lv_calendar_header_arrow$(ObjectSuffix) $(IntermediateDirectory)/calendar_lv_calendar_header_dropdown$(ObjectSuffix) $(IntermediateDirectory)/chart_lv_chart$(ObjectSuffix) $(IntermediateDirectory)/colorwheel_lv_colorwheel$(ObjectSuffix) 

Objects3=$(IntermediateDirectory)/imgbtn_lv_imgbtn$(ObjectSuffix) \
	$(IntermediateDirectory)/keyboard_lv_keyboard$(ObjectSuffix) $(IntermediateDirectory)/led_lv_led$(ObjectSuffix) $(IntermediateDirectory)/list_lv_list$(ObjectSuffix) $(IntermediateDirectory)/meter_lv_meter$(ObjectSuffix) $(IntermediateDirectory)/msgbox_lv_msgbox$(ObjectSuffix) $(IntermediateDirectory)/span_lv_span$(ObjectSuffix) $(IntermediateDirectory)/spinbox_lv_spinbox$(ObjectSuffix) $(IntermediateDirectory)/spinner_lv_spinner$(ObjectSuffix) $(IntermediateDirectory)/tabview_lv_tabview$(ObjectSuffix) $(IntermediateDirectory)/tileview_lv_tileview$(ObjectSuffix) \
	$(IntermediateDirectory)/win_lv_win$(ObjectSuffix) $(IntermediateDirectory)/assets_img_clothes$(ObjectSuffix) $(IntermediateDirectory)/assets_img_demo_widgets_avatar$(ObjectSuffix) $(IntermediateDirectory)/assets_img_lvgl_logo$(ObjectSuffix) $(IntermediateDirectory)/__rt_entry$(ObjectSuffix) 



Objects=$(Objects0) $(Objects1) $(Objects2) $(Objects3) 

##
## Main Build Targets 
##
.PHONY: all
all: $(IntermediateDirectory)/$(OutputFile)

$(IntermediateDirectory)/$(OutputFile):  $(Objects) Always_Link 
	$(LinkerName) $(OutputSwitch) $(IntermediateDirectory)/$(OutputFile)$(ExeSuffix) $(LinkerNameoption) $(LinkOtherFlagsOption)  -Wl,--ckmap=$(ProjectPath)/Lst/$(OutputFile).map  @$(ObjectsFileList)  $(LinkOptions) $(LibPath) $(Libs)
	@mv $(ProjectPath)/Lst/$(OutputFile).map $(ProjectPath)/Lst/$(OutputFile).temp && $(READELF) $(ElfInfoSwitch) $(ProjectPath)/Obj/$(OutputFile)$(ExeSuffix) > $(ProjectPath)/Lst/$(OutputFile).map && echo ====================================================================== >> $(ProjectPath)/Lst/$(OutputFile).map && cat $(ProjectPath)/Lst/$(OutputFile).temp >> $(ProjectPath)/Lst/$(OutputFile).map && rm -rf $(ProjectPath)/Lst/$(OutputFile).temp
	$(OBJDUMP) $(ObjdumpSwitch) $(ProjectPath)/$(IntermediateDirectory)/$(OutputFile)$(ExeSuffix)  > $(ProjectPath)/Lst/$(OutputFile)$(DisassemSuffix) 
	@echo size of target:
	@$(SIZE) $(ProjectPath)$(IntermediateDirectory)/$(OutputFile)$(ExeSuffix) 
	@echo -n checksum value of target:  
	@$(CHECKSUM) $(ProjectPath)/$(IntermediateDirectory)/$(OutputFile)$(ExeSuffix) 
	@$(ProjectName).modify.bat $(IntermediateDirectory) $(OutputFile)$(ExeSuffix) 

Always_Link:


##
## Objects
##
$(IntermediateDirectory)/src_main$(ObjectSuffix): src/main.c  
	$(CC) $(SourceSwitch) src/main.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/src_main$(ObjectSuffix) -MF$(IntermediateDirectory)/src_main$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/src_main$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/src_main$(PreprocessSuffix): src/main.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/src_main$(PreprocessSuffix) src/main.c

$(IntermediateDirectory)/src_wm_hal_msp$(ObjectSuffix): src/wm_hal_msp.c  
	$(CC) $(SourceSwitch) src/wm_hal_msp.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/src_wm_hal_msp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_wm_hal_msp$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/src_wm_hal_msp$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/src_wm_hal_msp$(PreprocessSuffix): src/wm_hal_msp.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/src_wm_hal_msp$(PreprocessSuffix) src/wm_hal_msp.c

$(IntermediateDirectory)/src_wm_it$(ObjectSuffix): src/wm_it.c  
	$(CC) $(SourceSwitch) src/wm_it.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/src_wm_it$(ObjectSuffix) -MF$(IntermediateDirectory)/src_wm_it$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/src_wm_it$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/src_wm_it$(PreprocessSuffix): src/wm_it.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/src_wm_it$(PreprocessSuffix) src/wm_it.c

$(IntermediateDirectory)/src_lcd$(ObjectSuffix): src/lcd.c  
	$(CC) $(SourceSwitch) src/lcd.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/src_lcd$(ObjectSuffix) -MF$(IntermediateDirectory)/src_lcd$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/src_lcd$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/src_lcd$(PreprocessSuffix): src/lcd.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/src_lcd$(PreprocessSuffix) src/lcd.c

$(IntermediateDirectory)/src_st7789_parallel$(ObjectSuffix): src/st7789_parallel.c  
	$(CC) $(SourceSwitch) src/st7789_parallel.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/src_st7789_parallel$(ObjectSuffix) -MF$(IntermediateDirectory)/src_st7789_parallel$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/src_st7789_parallel$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/src_st7789_parallel$(PreprocessSuffix): src/st7789_parallel.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/src_st7789_parallel$(PreprocessSuffix) src/st7789_parallel.c

$(IntermediateDirectory)/drivers_wm_adc$(ObjectSuffix): ../Libraries/drivers/wm_adc.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_adc.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_adc$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_adc$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_adc$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_adc$(PreprocessSuffix): ../Libraries/drivers/wm_adc.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_adc$(PreprocessSuffix) ../Libraries/drivers/wm_adc.c

$(IntermediateDirectory)/drivers_wm_cpu$(ObjectSuffix): ../Libraries/drivers/wm_cpu.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_cpu.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_cpu$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_cpu$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_cpu$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_cpu$(PreprocessSuffix): ../Libraries/drivers/wm_cpu.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_cpu$(PreprocessSuffix) ../Libraries/drivers/wm_cpu.c

$(IntermediateDirectory)/drivers_wm_gpio$(ObjectSuffix): ../Libraries/drivers/wm_gpio.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_gpio.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_gpio$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_gpio$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_gpio$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_gpio$(PreprocessSuffix): ../Libraries/drivers/wm_gpio.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_gpio$(PreprocessSuffix) ../Libraries/drivers/wm_gpio.c

$(IntermediateDirectory)/drivers_wm_hal$(ObjectSuffix): ../Libraries/drivers/wm_hal.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_hal.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_hal$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_hal$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_hal$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_hal$(PreprocessSuffix): ../Libraries/drivers/wm_hal.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_hal$(PreprocessSuffix) ../Libraries/drivers/wm_hal.c

$(IntermediateDirectory)/drivers_wm_i2c$(ObjectSuffix): ../Libraries/drivers/wm_i2c.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_i2c.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_i2c$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_i2c$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_i2c$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_i2c$(PreprocessSuffix): ../Libraries/drivers/wm_i2c.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_i2c$(PreprocessSuffix) ../Libraries/drivers/wm_i2c.c

$(IntermediateDirectory)/drivers_wm_internal_flash$(ObjectSuffix): ../Libraries/drivers/wm_internal_flash.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_internal_flash.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_internal_flash$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_internal_flash$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_internal_flash$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_internal_flash$(PreprocessSuffix): ../Libraries/drivers/wm_internal_flash.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_internal_flash$(PreprocessSuffix) ../Libraries/drivers/wm_internal_flash.c

$(IntermediateDirectory)/drivers_wm_pmu$(ObjectSuffix): ../Libraries/drivers/wm_pmu.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_pmu.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_pmu$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_pmu$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_pmu$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_pmu$(PreprocessSuffix): ../Libraries/drivers/wm_pmu.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_pmu$(PreprocessSuffix) ../Libraries/drivers/wm_pmu.c

$(IntermediateDirectory)/drivers_wm_pwm$(ObjectSuffix): ../Libraries/drivers/wm_pwm.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_pwm.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_pwm$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_pwm$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_pwm$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_pwm$(PreprocessSuffix): ../Libraries/drivers/wm_pwm.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_pwm$(PreprocessSuffix) ../Libraries/drivers/wm_pwm.c

$(IntermediateDirectory)/drivers_wm_spi$(ObjectSuffix): ../Libraries/drivers/wm_spi.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_spi.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_spi$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_spi$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_spi$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_spi$(PreprocessSuffix): ../Libraries/drivers/wm_spi.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_spi$(PreprocessSuffix) ../Libraries/drivers/wm_spi.c

$(IntermediateDirectory)/drivers_wm_spi_flash$(ObjectSuffix): ../Libraries/drivers/wm_spi_flash.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_spi_flash.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_spi_flash$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_spi_flash$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_spi_flash$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_spi_flash$(PreprocessSuffix): ../Libraries/drivers/wm_spi_flash.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_spi_flash$(PreprocessSuffix) ../Libraries/drivers/wm_spi_flash.c

$(IntermediateDirectory)/drivers_wm_tim$(ObjectSuffix): ../Libraries/drivers/wm_tim.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_tim.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_tim$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_tim$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_tim$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_tim$(PreprocessSuffix): ../Libraries/drivers/wm_tim.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_tim$(PreprocessSuffix) ../Libraries/drivers/wm_tim.c

$(IntermediateDirectory)/drivers_wm_touch$(ObjectSuffix): ../Libraries/drivers/wm_touch.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_touch.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_touch$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_touch$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_touch$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_touch$(PreprocessSuffix): ../Libraries/drivers/wm_touch.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_touch$(PreprocessSuffix) ../Libraries/drivers/wm_touch.c

$(IntermediateDirectory)/drivers_wm_uart$(ObjectSuffix): ../Libraries/drivers/wm_uart.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_uart.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_uart$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_uart$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_uart$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_uart$(PreprocessSuffix): ../Libraries/drivers/wm_uart.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_uart$(PreprocessSuffix) ../Libraries/drivers/wm_uart.c

$(IntermediateDirectory)/drivers_wm_wdg$(ObjectSuffix): ../Libraries/drivers/wm_wdg.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_wdg.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_wdg$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_wdg$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_wdg$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_wdg$(PreprocessSuffix): ../Libraries/drivers/wm_wdg.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_wdg$(PreprocessSuffix) ../Libraries/drivers/wm_wdg.c

$(IntermediateDirectory)/drivers_wm_lcd$(ObjectSuffix): ../Libraries/drivers/wm_lcd.c  
	$(CC) $(SourceSwitch) ../Libraries/drivers/wm_lcd.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/drivers_wm_lcd$(ObjectSuffix) -MF$(IntermediateDirectory)/drivers_wm_lcd$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/drivers_wm_lcd$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/drivers_wm_lcd$(PreprocessSuffix): ../Libraries/drivers/wm_lcd.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/drivers_wm_lcd$(PreprocessSuffix) ../Libraries/drivers/wm_lcd.c

$(IntermediateDirectory)/port_lv_port_disp$(ObjectSuffix): ../lgvlV8/port/lv_port_disp.c  
	$(CC) $(SourceSwitch) ../lgvlV8/port/lv_port_disp.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/port_lv_port_disp$(ObjectSuffix) -MF$(IntermediateDirectory)/port_lv_port_disp$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/port_lv_port_disp$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/port_lv_port_disp$(PreprocessSuffix): ../lgvlV8/port/lv_port_disp.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/port_lv_port_disp$(PreprocessSuffix) ../lgvlV8/port/lv_port_disp.c

$(IntermediateDirectory)/core_lv_disp$(ObjectSuffix): ../lgvlV8/src/core/lv_disp.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_disp.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_disp$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_disp$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_disp$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_disp$(PreprocessSuffix): ../lgvlV8/src/core/lv_disp.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_disp$(PreprocessSuffix) ../lgvlV8/src/core/lv_disp.c

$(IntermediateDirectory)/core_lv_event$(ObjectSuffix): ../lgvlV8/src/core/lv_event.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_event.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_event$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_event$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_event$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_event$(PreprocessSuffix): ../lgvlV8/src/core/lv_event.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_event$(PreprocessSuffix) ../lgvlV8/src/core/lv_event.c

$(IntermediateDirectory)/core_lv_group$(ObjectSuffix): ../lgvlV8/src/core/lv_group.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_group.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_group$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_group$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_group$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_group$(PreprocessSuffix): ../lgvlV8/src/core/lv_group.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_group$(PreprocessSuffix) ../lgvlV8/src/core/lv_group.c

$(IntermediateDirectory)/core_lv_indev$(ObjectSuffix): ../lgvlV8/src/core/lv_indev.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_indev.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_indev$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_indev$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_indev$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_indev$(PreprocessSuffix): ../lgvlV8/src/core/lv_indev.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_indev$(PreprocessSuffix) ../lgvlV8/src/core/lv_indev.c

$(IntermediateDirectory)/core_lv_indev_scroll$(ObjectSuffix): ../lgvlV8/src/core/lv_indev_scroll.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_indev_scroll.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_indev_scroll$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_indev_scroll$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_indev_scroll$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_indev_scroll$(PreprocessSuffix): ../lgvlV8/src/core/lv_indev_scroll.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_indev_scroll$(PreprocessSuffix) ../lgvlV8/src/core/lv_indev_scroll.c

$(IntermediateDirectory)/core_lv_obj$(ObjectSuffix): ../lgvlV8/src/core/lv_obj.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj.c

$(IntermediateDirectory)/core_lv_obj_class$(ObjectSuffix): ../lgvlV8/src/core/lv_obj_class.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj_class.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj_class$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj_class$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj_class$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj_class$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj_class.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj_class$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj_class.c

$(IntermediateDirectory)/core_lv_obj_draw$(ObjectSuffix): ../lgvlV8/src/core/lv_obj_draw.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj_draw.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj_draw$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj_draw$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj_draw$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj_draw$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj_draw.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj_draw$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj_draw.c

$(IntermediateDirectory)/core_lv_obj_pos$(ObjectSuffix): ../lgvlV8/src/core/lv_obj_pos.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj_pos.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj_pos$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj_pos$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj_pos$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj_pos$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj_pos.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj_pos$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj_pos.c

$(IntermediateDirectory)/core_lv_obj_scroll$(ObjectSuffix): ../lgvlV8/src/core/lv_obj_scroll.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj_scroll.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj_scroll$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj_scroll$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj_scroll$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj_scroll$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj_scroll.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj_scroll$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj_scroll.c

$(IntermediateDirectory)/core_lv_obj_style$(ObjectSuffix): ../lgvlV8/src/core/lv_obj_style.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj_style.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj_style$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj_style$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj_style$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj_style$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj_style.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj_style$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj_style.c

$(IntermediateDirectory)/core_lv_obj_style_gen$(ObjectSuffix): ../lgvlV8/src/core/lv_obj_style_gen.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj_style_gen.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj_style_gen$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj_style_gen$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj_style_gen$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj_style_gen$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj_style_gen.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj_style_gen$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj_style_gen.c

$(IntermediateDirectory)/core_lv_obj_tree$(ObjectSuffix): ../lgvlV8/src/core/lv_obj_tree.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_obj_tree.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_obj_tree$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_obj_tree$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_obj_tree$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_obj_tree$(PreprocessSuffix): ../lgvlV8/src/core/lv_obj_tree.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_obj_tree$(PreprocessSuffix) ../lgvlV8/src/core/lv_obj_tree.c

$(IntermediateDirectory)/core_lv_refr$(ObjectSuffix): ../lgvlV8/src/core/lv_refr.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_refr.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_refr$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_refr$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_refr$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_refr$(PreprocessSuffix): ../lgvlV8/src/core/lv_refr.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_refr$(PreprocessSuffix) ../lgvlV8/src/core/lv_refr.c

$(IntermediateDirectory)/core_lv_theme$(ObjectSuffix): ../lgvlV8/src/core/lv_theme.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/core/lv_theme.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/core_lv_theme$(ObjectSuffix) -MF$(IntermediateDirectory)/core_lv_theme$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/core_lv_theme$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/core_lv_theme$(PreprocessSuffix): ../lgvlV8/src/core/lv_theme.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/core_lv_theme$(PreprocessSuffix) ../lgvlV8/src/core/lv_theme.c

$(IntermediateDirectory)/draw_lv_draw_arc$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_arc.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_arc.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_arc$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_arc$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_arc$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_arc$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_arc.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_arc$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_arc.c

$(IntermediateDirectory)/draw_lv_draw_blend$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_blend.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_blend.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_blend$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_blend$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_blend$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_blend$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_blend.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_blend$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_blend.c

$(IntermediateDirectory)/draw_lv_draw_img$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_img.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_img.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_img$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_img$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_img$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_img$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_img.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_img$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_img.c

$(IntermediateDirectory)/draw_lv_draw_label$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_label.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_label.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_label$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_label$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_label$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_label$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_label.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_label$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_label.c

$(IntermediateDirectory)/draw_lv_draw_line$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_line.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_line.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_line$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_line$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_line$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_line$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_line.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_line$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_line.c

$(IntermediateDirectory)/draw_lv_draw_mask$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_mask.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_mask.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_mask$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_mask$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_mask$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_mask$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_mask.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_mask$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_mask.c

$(IntermediateDirectory)/draw_lv_draw_rect$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_rect.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_rect.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_rect$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_rect$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_rect$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_rect$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_rect.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_rect$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_rect.c

$(IntermediateDirectory)/draw_lv_draw_triangle$(ObjectSuffix): ../lgvlV8/src/draw/lv_draw_triangle.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_draw_triangle.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_draw_triangle$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_draw_triangle$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_draw_triangle$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_draw_triangle$(PreprocessSuffix): ../lgvlV8/src/draw/lv_draw_triangle.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_draw_triangle$(PreprocessSuffix) ../lgvlV8/src/draw/lv_draw_triangle.c

$(IntermediateDirectory)/draw_lv_img_buf$(ObjectSuffix): ../lgvlV8/src/draw/lv_img_buf.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_img_buf.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_img_buf$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_img_buf$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_img_buf$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_img_buf$(PreprocessSuffix): ../lgvlV8/src/draw/lv_img_buf.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_img_buf$(PreprocessSuffix) ../lgvlV8/src/draw/lv_img_buf.c

$(IntermediateDirectory)/draw_lv_img_cache$(ObjectSuffix): ../lgvlV8/src/draw/lv_img_cache.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_img_cache.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_img_cache$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_img_cache$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_img_cache$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_img_cache$(PreprocessSuffix): ../lgvlV8/src/draw/lv_img_cache.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_img_cache$(PreprocessSuffix) ../lgvlV8/src/draw/lv_img_cache.c

$(IntermediateDirectory)/draw_lv_img_decoder$(ObjectSuffix): ../lgvlV8/src/draw/lv_img_decoder.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/draw/lv_img_decoder.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/draw_lv_img_decoder$(ObjectSuffix) -MF$(IntermediateDirectory)/draw_lv_img_decoder$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/draw_lv_img_decoder$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/draw_lv_img_decoder$(PreprocessSuffix): ../lgvlV8/src/draw/lv_img_decoder.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/draw_lv_img_decoder$(PreprocessSuffix) ../lgvlV8/src/draw/lv_img_decoder.c

$(IntermediateDirectory)/extra_lv_extra$(ObjectSuffix): ../lgvlV8/src/extra/lv_extra.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/lv_extra.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/extra_lv_extra$(ObjectSuffix) -MF$(IntermediateDirectory)/extra_lv_extra$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/extra_lv_extra$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/extra_lv_extra$(PreprocessSuffix): ../lgvlV8/src/extra/lv_extra.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/extra_lv_extra$(PreprocessSuffix) ../lgvlV8/src/extra/lv_extra.c

$(IntermediateDirectory)/font_lv_font$(ObjectSuffix): ../lgvlV8/src/font/lv_font.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font$(PreprocessSuffix): ../lgvlV8/src/font/lv_font.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font$(PreprocessSuffix) ../lgvlV8/src/font/lv_font.c

$(IntermediateDirectory)/font_lv_font_dejavu_16_persian_hebrew$(ObjectSuffix): ../lgvlV8/src/font/lv_font_dejavu_16_persian_hebrew.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_dejavu_16_persian_hebrew.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_dejavu_16_persian_hebrew$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_dejavu_16_persian_hebrew$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_dejavu_16_persian_hebrew$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_dejavu_16_persian_hebrew$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_dejavu_16_persian_hebrew.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_dejavu_16_persian_hebrew$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_dejavu_16_persian_hebrew.c

$(IntermediateDirectory)/font_lv_font_fmt_txt$(ObjectSuffix): ../lgvlV8/src/font/lv_font_fmt_txt.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_fmt_txt.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_fmt_txt$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_fmt_txt$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_fmt_txt$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_fmt_txt$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_fmt_txt.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_fmt_txt$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_fmt_txt.c

$(IntermediateDirectory)/font_lv_font_loader$(ObjectSuffix): ../lgvlV8/src/font/lv_font_loader.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_loader.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_loader$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_loader$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_loader$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_loader$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_loader.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_loader$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_loader.c

$(IntermediateDirectory)/font_lv_font_montserrat_10$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_10.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_10.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_10$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_10$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_10$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_10$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_10.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_10$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_10.c

$(IntermediateDirectory)/font_lv_font_montserrat_12$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_12.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_12.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_12$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_12$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_12$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_12$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_12.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_12$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_12.c

$(IntermediateDirectory)/font_lv_font_montserrat_12_subpx$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_12_subpx.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_12_subpx.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_12_subpx$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_12_subpx$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_12_subpx$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_12_subpx$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_12_subpx.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_12_subpx$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_12_subpx.c

$(IntermediateDirectory)/font_lv_font_montserrat_14$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_14.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_14.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_14$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_14$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_14$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_14$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_14.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_14$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_14.c

$(IntermediateDirectory)/font_lv_font_montserrat_16$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_16.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_16.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_16$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_16$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_16$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_16$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_16.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_16$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_16.c

$(IntermediateDirectory)/font_lv_font_montserrat_18$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_18.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_18.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_18$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_18$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_18$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_18$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_18.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_18$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_18.c

$(IntermediateDirectory)/font_lv_font_montserrat_20$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_20.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_20.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_20$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_20$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_20$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_20$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_20.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_20$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_20.c

$(IntermediateDirectory)/font_lv_font_montserrat_22$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_22.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_22.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_22$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_22$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_22$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_22$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_22.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_22$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_22.c

$(IntermediateDirectory)/font_lv_font_montserrat_24$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_24.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_24.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_24$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_24$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_24$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_24$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_24.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_24$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_24.c

$(IntermediateDirectory)/font_lv_font_montserrat_26$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_26.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_26.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_26$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_26$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_26$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_26$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_26.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_26$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_26.c

$(IntermediateDirectory)/font_lv_font_montserrat_28$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_28.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_28.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_28$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_28$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_28$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_28$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_28.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_28$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_28.c

$(IntermediateDirectory)/font_lv_font_montserrat_28_compressed$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_28_compressed.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_28_compressed.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_28_compressed$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_28_compressed$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_28_compressed$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_28_compressed$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_28_compressed.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_28_compressed$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_28_compressed.c

$(IntermediateDirectory)/font_lv_font_montserrat_30$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_30.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_30.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_30$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_30$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_30$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_30$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_30.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_30$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_30.c

$(IntermediateDirectory)/font_lv_font_montserrat_32$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_32.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_32.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_32$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_32$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_32$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_32$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_32.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_32$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_32.c

$(IntermediateDirectory)/font_lv_font_montserrat_34$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_34.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_34.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_34$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_34$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_34$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_34$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_34.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_34$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_34.c

$(IntermediateDirectory)/font_lv_font_montserrat_36$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_36.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_36.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_36$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_36$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_36$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_36$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_36.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_36$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_36.c

$(IntermediateDirectory)/font_lv_font_montserrat_38$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_38.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_38.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_38$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_38$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_38$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_38$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_38.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_38$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_38.c

$(IntermediateDirectory)/font_lv_font_montserrat_40$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_40.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_40.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_40$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_40$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_40$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_40$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_40.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_40$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_40.c

$(IntermediateDirectory)/font_lv_font_montserrat_42$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_42.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_42.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_42$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_42$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_42$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_42$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_42.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_42$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_42.c

$(IntermediateDirectory)/font_lv_font_montserrat_44$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_44.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_44.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_44$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_44$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_44$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_44$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_44.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_44$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_44.c

$(IntermediateDirectory)/font_lv_font_montserrat_46$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_46.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_46.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_46$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_46$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_46$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_46$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_46.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_46$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_46.c

$(IntermediateDirectory)/font_lv_font_montserrat_48$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_48.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_48.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_48$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_48$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_48$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_48$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_48.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_48$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_48.c

$(IntermediateDirectory)/font_lv_font_montserrat_8$(ObjectSuffix): ../lgvlV8/src/font/lv_font_montserrat_8.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_montserrat_8.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_montserrat_8$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_montserrat_8$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_montserrat_8$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_montserrat_8$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_montserrat_8.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_montserrat_8$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_montserrat_8.c

$(IntermediateDirectory)/font_lv_font_simsun_16_cjk$(ObjectSuffix): ../lgvlV8/src/font/lv_font_simsun_16_cjk.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_simsun_16_cjk.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_simsun_16_cjk$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_simsun_16_cjk$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_simsun_16_cjk$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_simsun_16_cjk$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_simsun_16_cjk.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_simsun_16_cjk$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_simsun_16_cjk.c

$(IntermediateDirectory)/font_lv_font_unscii_16$(ObjectSuffix): ../lgvlV8/src/font/lv_font_unscii_16.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_unscii_16.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_unscii_16$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_unscii_16$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_unscii_16$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_unscii_16$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_unscii_16.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_unscii_16$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_unscii_16.c

$(IntermediateDirectory)/font_lv_font_unscii_8$(ObjectSuffix): ../lgvlV8/src/font/lv_font_unscii_8.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/font/lv_font_unscii_8.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/font_lv_font_unscii_8$(ObjectSuffix) -MF$(IntermediateDirectory)/font_lv_font_unscii_8$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/font_lv_font_unscii_8$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/font_lv_font_unscii_8$(PreprocessSuffix): ../lgvlV8/src/font/lv_font_unscii_8.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/font_lv_font_unscii_8$(PreprocessSuffix) ../lgvlV8/src/font/lv_font_unscii_8.c

$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp$(ObjectSuffix): ../lgvlV8/src/gpu/lv_gpu_nxp_pxp.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/gpu/lv_gpu_nxp_pxp.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp$(ObjectSuffix) -MF$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/gpu_lv_gpu_nxp_pxp$(PreprocessSuffix): ../lgvlV8/src/gpu/lv_gpu_nxp_pxp.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/gpu_lv_gpu_nxp_pxp$(PreprocessSuffix) ../lgvlV8/src/gpu/lv_gpu_nxp_pxp.c

$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp_osa$(ObjectSuffix): ../lgvlV8/src/gpu/lv_gpu_nxp_pxp_osa.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/gpu/lv_gpu_nxp_pxp_osa.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp_osa$(ObjectSuffix) -MF$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp_osa$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/gpu_lv_gpu_nxp_pxp_osa$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/gpu_lv_gpu_nxp_pxp_osa$(PreprocessSuffix): ../lgvlV8/src/gpu/lv_gpu_nxp_pxp_osa.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/gpu_lv_gpu_nxp_pxp_osa$(PreprocessSuffix) ../lgvlV8/src/gpu/lv_gpu_nxp_pxp_osa.c

$(IntermediateDirectory)/gpu_lv_gpu_nxp_vglite$(ObjectSuffix): ../lgvlV8/src/gpu/lv_gpu_nxp_vglite.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/gpu/lv_gpu_nxp_vglite.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/gpu_lv_gpu_nxp_vglite$(ObjectSuffix) -MF$(IntermediateDirectory)/gpu_lv_gpu_nxp_vglite$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/gpu_lv_gpu_nxp_vglite$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/gpu_lv_gpu_nxp_vglite$(PreprocessSuffix): ../lgvlV8/src/gpu/lv_gpu_nxp_vglite.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/gpu_lv_gpu_nxp_vglite$(PreprocessSuffix) ../lgvlV8/src/gpu/lv_gpu_nxp_vglite.c

$(IntermediateDirectory)/gpu_lv_gpu_stm32_dma2d$(ObjectSuffix): ../lgvlV8/src/gpu/lv_gpu_stm32_dma2d.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/gpu/lv_gpu_stm32_dma2d.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/gpu_lv_gpu_stm32_dma2d$(ObjectSuffix) -MF$(IntermediateDirectory)/gpu_lv_gpu_stm32_dma2d$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/gpu_lv_gpu_stm32_dma2d$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/gpu_lv_gpu_stm32_dma2d$(PreprocessSuffix): ../lgvlV8/src/gpu/lv_gpu_stm32_dma2d.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/gpu_lv_gpu_stm32_dma2d$(PreprocessSuffix) ../lgvlV8/src/gpu/lv_gpu_stm32_dma2d.c

$(IntermediateDirectory)/hal_lv_hal_disp$(ObjectSuffix): ../lgvlV8/src/hal/lv_hal_disp.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/hal/lv_hal_disp.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/hal_lv_hal_disp$(ObjectSuffix) -MF$(IntermediateDirectory)/hal_lv_hal_disp$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/hal_lv_hal_disp$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/hal_lv_hal_disp$(PreprocessSuffix): ../lgvlV8/src/hal/lv_hal_disp.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/hal_lv_hal_disp$(PreprocessSuffix) ../lgvlV8/src/hal/lv_hal_disp.c

$(IntermediateDirectory)/hal_lv_hal_indev$(ObjectSuffix): ../lgvlV8/src/hal/lv_hal_indev.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/hal/lv_hal_indev.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/hal_lv_hal_indev$(ObjectSuffix) -MF$(IntermediateDirectory)/hal_lv_hal_indev$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/hal_lv_hal_indev$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/hal_lv_hal_indev$(PreprocessSuffix): ../lgvlV8/src/hal/lv_hal_indev.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/hal_lv_hal_indev$(PreprocessSuffix) ../lgvlV8/src/hal/lv_hal_indev.c

$(IntermediateDirectory)/hal_lv_hal_tick$(ObjectSuffix): ../lgvlV8/src/hal/lv_hal_tick.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/hal/lv_hal_tick.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/hal_lv_hal_tick$(ObjectSuffix) -MF$(IntermediateDirectory)/hal_lv_hal_tick$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/hal_lv_hal_tick$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/hal_lv_hal_tick$(PreprocessSuffix): ../lgvlV8/src/hal/lv_hal_tick.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/hal_lv_hal_tick$(PreprocessSuffix) ../lgvlV8/src/hal/lv_hal_tick.c

$(IntermediateDirectory)/misc_lv_anim$(ObjectSuffix): ../lgvlV8/src/misc/lv_anim.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_anim.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_anim$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_anim$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_anim$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_anim$(PreprocessSuffix): ../lgvlV8/src/misc/lv_anim.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_anim$(PreprocessSuffix) ../lgvlV8/src/misc/lv_anim.c

$(IntermediateDirectory)/misc_lv_area$(ObjectSuffix): ../lgvlV8/src/misc/lv_area.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_area.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_area$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_area$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_area$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_area$(PreprocessSuffix): ../lgvlV8/src/misc/lv_area.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_area$(PreprocessSuffix) ../lgvlV8/src/misc/lv_area.c

$(IntermediateDirectory)/misc_lv_async$(ObjectSuffix): ../lgvlV8/src/misc/lv_async.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_async.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_async$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_async$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_async$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_async$(PreprocessSuffix): ../lgvlV8/src/misc/lv_async.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_async$(PreprocessSuffix) ../lgvlV8/src/misc/lv_async.c

$(IntermediateDirectory)/misc_lv_bidi$(ObjectSuffix): ../lgvlV8/src/misc/lv_bidi.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_bidi.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_bidi$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_bidi$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_bidi$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_bidi$(PreprocessSuffix): ../lgvlV8/src/misc/lv_bidi.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_bidi$(PreprocessSuffix) ../lgvlV8/src/misc/lv_bidi.c

$(IntermediateDirectory)/misc_lv_color$(ObjectSuffix): ../lgvlV8/src/misc/lv_color.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_color.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_color$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_color$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_color$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_color$(PreprocessSuffix): ../lgvlV8/src/misc/lv_color.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_color$(PreprocessSuffix) ../lgvlV8/src/misc/lv_color.c

$(IntermediateDirectory)/misc_lv_fs$(ObjectSuffix): ../lgvlV8/src/misc/lv_fs.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_fs.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_fs$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_fs$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_fs$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_fs$(PreprocessSuffix): ../lgvlV8/src/misc/lv_fs.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_fs$(PreprocessSuffix) ../lgvlV8/src/misc/lv_fs.c

$(IntermediateDirectory)/misc_lv_gc$(ObjectSuffix): ../lgvlV8/src/misc/lv_gc.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_gc.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_gc$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_gc$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_gc$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_gc$(PreprocessSuffix): ../lgvlV8/src/misc/lv_gc.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_gc$(PreprocessSuffix) ../lgvlV8/src/misc/lv_gc.c

$(IntermediateDirectory)/misc_lv_ll$(ObjectSuffix): ../lgvlV8/src/misc/lv_ll.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_ll.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_ll$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_ll$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_ll$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_ll$(PreprocessSuffix): ../lgvlV8/src/misc/lv_ll.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_ll$(PreprocessSuffix) ../lgvlV8/src/misc/lv_ll.c

$(IntermediateDirectory)/misc_lv_log$(ObjectSuffix): ../lgvlV8/src/misc/lv_log.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_log.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_log$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_log$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_log$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_log$(PreprocessSuffix): ../lgvlV8/src/misc/lv_log.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_log$(PreprocessSuffix) ../lgvlV8/src/misc/lv_log.c

$(IntermediateDirectory)/misc_lv_math$(ObjectSuffix): ../lgvlV8/src/misc/lv_math.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_math.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_math$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_math$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_math$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_math$(PreprocessSuffix): ../lgvlV8/src/misc/lv_math.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_math$(PreprocessSuffix) ../lgvlV8/src/misc/lv_math.c

$(IntermediateDirectory)/misc_lv_mem$(ObjectSuffix): ../lgvlV8/src/misc/lv_mem.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_mem.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_mem$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_mem$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_mem$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_mem$(PreprocessSuffix): ../lgvlV8/src/misc/lv_mem.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_mem$(PreprocessSuffix) ../lgvlV8/src/misc/lv_mem.c

$(IntermediateDirectory)/misc_lv_printf$(ObjectSuffix): ../lgvlV8/src/misc/lv_printf.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_printf.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_printf$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_printf$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_printf$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_printf$(PreprocessSuffix): ../lgvlV8/src/misc/lv_printf.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_printf$(PreprocessSuffix) ../lgvlV8/src/misc/lv_printf.c

$(IntermediateDirectory)/misc_lv_style$(ObjectSuffix): ../lgvlV8/src/misc/lv_style.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_style.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_style$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_style$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_style$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_style$(PreprocessSuffix): ../lgvlV8/src/misc/lv_style.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_style$(PreprocessSuffix) ../lgvlV8/src/misc/lv_style.c

$(IntermediateDirectory)/misc_lv_style_gen$(ObjectSuffix): ../lgvlV8/src/misc/lv_style_gen.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_style_gen.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_style_gen$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_style_gen$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_style_gen$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_style_gen$(PreprocessSuffix): ../lgvlV8/src/misc/lv_style_gen.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_style_gen$(PreprocessSuffix) ../lgvlV8/src/misc/lv_style_gen.c

$(IntermediateDirectory)/misc_lv_templ$(ObjectSuffix): ../lgvlV8/src/misc/lv_templ.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_templ.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_templ$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_templ$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_templ$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_templ$(PreprocessSuffix): ../lgvlV8/src/misc/lv_templ.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_templ$(PreprocessSuffix) ../lgvlV8/src/misc/lv_templ.c

$(IntermediateDirectory)/misc_lv_timer$(ObjectSuffix): ../lgvlV8/src/misc/lv_timer.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_timer.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_timer$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_timer$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_timer$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_timer$(PreprocessSuffix): ../lgvlV8/src/misc/lv_timer.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_timer$(PreprocessSuffix) ../lgvlV8/src/misc/lv_timer.c

$(IntermediateDirectory)/misc_lv_tlsf$(ObjectSuffix): ../lgvlV8/src/misc/lv_tlsf.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_tlsf.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_tlsf$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_tlsf$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_tlsf$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_tlsf$(PreprocessSuffix): ../lgvlV8/src/misc/lv_tlsf.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_tlsf$(PreprocessSuffix) ../lgvlV8/src/misc/lv_tlsf.c

$(IntermediateDirectory)/misc_lv_txt$(ObjectSuffix): ../lgvlV8/src/misc/lv_txt.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_txt.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_txt$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_txt$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_txt$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_txt$(PreprocessSuffix): ../lgvlV8/src/misc/lv_txt.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_txt$(PreprocessSuffix) ../lgvlV8/src/misc/lv_txt.c

$(IntermediateDirectory)/misc_lv_txt_ap$(ObjectSuffix): ../lgvlV8/src/misc/lv_txt_ap.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_txt_ap.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_txt_ap$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_txt_ap$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_txt_ap$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_txt_ap$(PreprocessSuffix): ../lgvlV8/src/misc/lv_txt_ap.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_txt_ap$(PreprocessSuffix) ../lgvlV8/src/misc/lv_txt_ap.c

$(IntermediateDirectory)/misc_lv_utils$(ObjectSuffix): ../lgvlV8/src/misc/lv_utils.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/misc/lv_utils.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/misc_lv_utils$(ObjectSuffix) -MF$(IntermediateDirectory)/misc_lv_utils$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/misc_lv_utils$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/misc_lv_utils$(PreprocessSuffix): ../lgvlV8/src/misc/lv_utils.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/misc_lv_utils$(PreprocessSuffix) ../lgvlV8/src/misc/lv_utils.c

$(IntermediateDirectory)/widgets_lv_arc$(ObjectSuffix): ../lgvlV8/src/widgets/lv_arc.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_arc.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_arc$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_arc$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_arc$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_arc$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_arc.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_arc$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_arc.c

$(IntermediateDirectory)/widgets_lv_bar$(ObjectSuffix): ../lgvlV8/src/widgets/lv_bar.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_bar.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_bar$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_bar$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_bar$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_bar$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_bar.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_bar$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_bar.c

$(IntermediateDirectory)/widgets_lv_btn$(ObjectSuffix): ../lgvlV8/src/widgets/lv_btn.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_btn.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_btn$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_btn$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_btn$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_btn$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_btn.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_btn$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_btn.c

$(IntermediateDirectory)/widgets_lv_btnmatrix$(ObjectSuffix): ../lgvlV8/src/widgets/lv_btnmatrix.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_btnmatrix.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_btnmatrix$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_btnmatrix$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_btnmatrix$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_btnmatrix$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_btnmatrix.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_btnmatrix$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_btnmatrix.c

$(IntermediateDirectory)/widgets_lv_canvas$(ObjectSuffix): ../lgvlV8/src/widgets/lv_canvas.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_canvas.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_canvas$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_canvas$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_canvas$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_canvas$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_canvas.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_canvas$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_canvas.c

$(IntermediateDirectory)/widgets_lv_checkbox$(ObjectSuffix): ../lgvlV8/src/widgets/lv_checkbox.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_checkbox.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_checkbox$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_checkbox$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_checkbox$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_checkbox$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_checkbox.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_checkbox$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_checkbox.c

$(IntermediateDirectory)/widgets_lv_dropdown$(ObjectSuffix): ../lgvlV8/src/widgets/lv_dropdown.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_dropdown.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_dropdown$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_dropdown$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_dropdown$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_dropdown$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_dropdown.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_dropdown$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_dropdown.c

$(IntermediateDirectory)/widgets_lv_img$(ObjectSuffix): ../lgvlV8/src/widgets/lv_img.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_img.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_img$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_img$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_img$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_img$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_img.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_img$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_img.c

$(IntermediateDirectory)/widgets_lv_label$(ObjectSuffix): ../lgvlV8/src/widgets/lv_label.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_label.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_label$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_label$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_label$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_label$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_label.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_label$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_label.c

$(IntermediateDirectory)/widgets_lv_line$(ObjectSuffix): ../lgvlV8/src/widgets/lv_line.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_line.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_line$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_line$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_line$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_line$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_line.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_line$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_line.c

$(IntermediateDirectory)/widgets_lv_objx_templ$(ObjectSuffix): ../lgvlV8/src/widgets/lv_objx_templ.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_objx_templ.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_objx_templ$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_objx_templ$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_objx_templ$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_objx_templ$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_objx_templ.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_objx_templ$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_objx_templ.c

$(IntermediateDirectory)/widgets_lv_roller$(ObjectSuffix): ../lgvlV8/src/widgets/lv_roller.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_roller.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_roller$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_roller$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_roller$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_roller$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_roller.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_roller$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_roller.c

$(IntermediateDirectory)/widgets_lv_slider$(ObjectSuffix): ../lgvlV8/src/widgets/lv_slider.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_slider.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_slider$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_slider$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_slider$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_slider$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_slider.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_slider$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_slider.c

$(IntermediateDirectory)/widgets_lv_switch$(ObjectSuffix): ../lgvlV8/src/widgets/lv_switch.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_switch.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_switch$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_switch$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_switch$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_switch$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_switch.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_switch$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_switch.c

$(IntermediateDirectory)/widgets_lv_table$(ObjectSuffix): ../lgvlV8/src/widgets/lv_table.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_table.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_table$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_table$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_table$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_table$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_table.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_table$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_table.c

$(IntermediateDirectory)/widgets_lv_textarea$(ObjectSuffix): ../lgvlV8/src/widgets/lv_textarea.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/widgets/lv_textarea.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/widgets_lv_textarea$(ObjectSuffix) -MF$(IntermediateDirectory)/widgets_lv_textarea$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/widgets_lv_textarea$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/widgets_lv_textarea$(PreprocessSuffix): ../lgvlV8/src/widgets/lv_textarea.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/widgets_lv_textarea$(PreprocessSuffix) ../lgvlV8/src/widgets/lv_textarea.c

$(IntermediateDirectory)/bsp_board_init$(ObjectSuffix): ../Libraries/arch/xt804/bsp/board_init.c  
	$(CC) $(SourceSwitch) ../Libraries/arch/xt804/bsp/board_init.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/bsp_board_init$(ObjectSuffix) -MF$(IntermediateDirectory)/bsp_board_init$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/bsp_board_init$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/bsp_board_init$(PreprocessSuffix): ../Libraries/arch/xt804/bsp/board_init.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/bsp_board_init$(PreprocessSuffix) ../Libraries/arch/xt804/bsp/board_init.c

$(IntermediateDirectory)/bsp_startup$(ObjectSuffix): ../Libraries/arch/xt804/bsp/startup.S  
	$(AS) $(SourceSwitch) ../Libraries/arch/xt804/bsp/startup.S $(ASFLAGS) -MMD -MP -MT$(IntermediateDirectory)/bsp_startup$(ObjectSuffix) -MF$(IntermediateDirectory)/bsp_startup$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/bsp_startup$(ObjectSuffix) $(IncludeAPath) $(IncludePackagePath)
Lst/bsp_startup$(PreprocessSuffix): ../Libraries/arch/xt804/bsp/startup.S
	$(CC) $(CFLAGS)$(IncludeAPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/bsp_startup$(PreprocessSuffix) ../Libraries/arch/xt804/bsp/startup.S

$(IntermediateDirectory)/bsp_system$(ObjectSuffix): ../Libraries/arch/xt804/bsp/system.c  
	$(CC) $(SourceSwitch) ../Libraries/arch/xt804/bsp/system.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/bsp_system$(ObjectSuffix) -MF$(IntermediateDirectory)/bsp_system$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/bsp_system$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/bsp_system$(PreprocessSuffix): ../Libraries/arch/xt804/bsp/system.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/bsp_system$(PreprocessSuffix) ../Libraries/arch/xt804/bsp/system.c

$(IntermediateDirectory)/bsp_trap_c$(ObjectSuffix): ../Libraries/arch/xt804/bsp/trap_c.c  
	$(CC) $(SourceSwitch) ../Libraries/arch/xt804/bsp/trap_c.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/bsp_trap_c$(ObjectSuffix) -MF$(IntermediateDirectory)/bsp_trap_c$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/bsp_trap_c$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/bsp_trap_c$(PreprocessSuffix): ../Libraries/arch/xt804/bsp/trap_c.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/bsp_trap_c$(PreprocessSuffix) ../Libraries/arch/xt804/bsp/trap_c.c

$(IntermediateDirectory)/bsp_vectors$(ObjectSuffix): ../Libraries/arch/xt804/bsp/vectors.S  
	$(AS) $(SourceSwitch) ../Libraries/arch/xt804/bsp/vectors.S $(ASFLAGS) -MMD -MP -MT$(IntermediateDirectory)/bsp_vectors$(ObjectSuffix) -MF$(IntermediateDirectory)/bsp_vectors$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/bsp_vectors$(ObjectSuffix) $(IncludeAPath) $(IncludePackagePath)
Lst/bsp_vectors$(PreprocessSuffix): ../Libraries/arch/xt804/bsp/vectors.S
	$(CC) $(CFLAGS)$(IncludeAPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/bsp_vectors$(PreprocessSuffix) ../Libraries/arch/xt804/bsp/vectors.S

$(IntermediateDirectory)/libc_libc_port$(ObjectSuffix): ../Libraries/arch/xt804/libc/libc_port.c  
	$(CC) $(SourceSwitch) ../Libraries/arch/xt804/libc/libc_port.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/libc_libc_port$(ObjectSuffix) -MF$(IntermediateDirectory)/libc_libc_port$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/libc_libc_port$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/libc_libc_port$(PreprocessSuffix): ../Libraries/arch/xt804/libc/libc_port.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/libc_libc_port$(PreprocessSuffix) ../Libraries/arch/xt804/libc/libc_port.c

$(IntermediateDirectory)/FreeRTOS_croutine$(ObjectSuffix): ../Libraries/component/FreeRTOS/croutine.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/croutine.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/FreeRTOS_croutine$(ObjectSuffix) -MF$(IntermediateDirectory)/FreeRTOS_croutine$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/FreeRTOS_croutine$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/FreeRTOS_croutine$(PreprocessSuffix): ../Libraries/component/FreeRTOS/croutine.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/FreeRTOS_croutine$(PreprocessSuffix) ../Libraries/component/FreeRTOS/croutine.c

$(IntermediateDirectory)/FreeRTOS_event_groups$(ObjectSuffix): ../Libraries/component/FreeRTOS/event_groups.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/event_groups.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/FreeRTOS_event_groups$(ObjectSuffix) -MF$(IntermediateDirectory)/FreeRTOS_event_groups$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/FreeRTOS_event_groups$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/FreeRTOS_event_groups$(PreprocessSuffix): ../Libraries/component/FreeRTOS/event_groups.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/FreeRTOS_event_groups$(PreprocessSuffix) ../Libraries/component/FreeRTOS/event_groups.c

$(IntermediateDirectory)/FreeRTOS_list$(ObjectSuffix): ../Libraries/component/FreeRTOS/list.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/list.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/FreeRTOS_list$(ObjectSuffix) -MF$(IntermediateDirectory)/FreeRTOS_list$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/FreeRTOS_list$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/FreeRTOS_list$(PreprocessSuffix): ../Libraries/component/FreeRTOS/list.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/FreeRTOS_list$(PreprocessSuffix) ../Libraries/component/FreeRTOS/list.c

$(IntermediateDirectory)/FreeRTOS_queue$(ObjectSuffix): ../Libraries/component/FreeRTOS/queue.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/queue.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/FreeRTOS_queue$(ObjectSuffix) -MF$(IntermediateDirectory)/FreeRTOS_queue$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/FreeRTOS_queue$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/FreeRTOS_queue$(PreprocessSuffix): ../Libraries/component/FreeRTOS/queue.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/FreeRTOS_queue$(PreprocessSuffix) ../Libraries/component/FreeRTOS/queue.c

$(IntermediateDirectory)/FreeRTOS_stream_buffer$(ObjectSuffix): ../Libraries/component/FreeRTOS/stream_buffer.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/stream_buffer.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/FreeRTOS_stream_buffer$(ObjectSuffix) -MF$(IntermediateDirectory)/FreeRTOS_stream_buffer$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/FreeRTOS_stream_buffer$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/FreeRTOS_stream_buffer$(PreprocessSuffix): ../Libraries/component/FreeRTOS/stream_buffer.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/FreeRTOS_stream_buffer$(PreprocessSuffix) ../Libraries/component/FreeRTOS/stream_buffer.c

$(IntermediateDirectory)/FreeRTOS_tasks$(ObjectSuffix): ../Libraries/component/FreeRTOS/tasks.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/tasks.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/FreeRTOS_tasks$(ObjectSuffix) -MF$(IntermediateDirectory)/FreeRTOS_tasks$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/FreeRTOS_tasks$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/FreeRTOS_tasks$(PreprocessSuffix): ../Libraries/component/FreeRTOS/tasks.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/FreeRTOS_tasks$(PreprocessSuffix) ../Libraries/component/FreeRTOS/tasks.c

$(IntermediateDirectory)/FreeRTOS_timers$(ObjectSuffix): ../Libraries/component/FreeRTOS/timers.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/timers.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/FreeRTOS_timers$(ObjectSuffix) -MF$(IntermediateDirectory)/FreeRTOS_timers$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/FreeRTOS_timers$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/FreeRTOS_timers$(PreprocessSuffix): ../Libraries/component/FreeRTOS/timers.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/FreeRTOS_timers$(PreprocessSuffix) ../Libraries/component/FreeRTOS/timers.c

$(IntermediateDirectory)/xt804_cpu_task_sw$(ObjectSuffix): ../Libraries/component/FreeRTOS/portable/xt804/cpu_task_sw.S  
	$(AS) $(SourceSwitch) ../Libraries/component/FreeRTOS/portable/xt804/cpu_task_sw.S $(ASFLAGS) -MMD -MP -MT$(IntermediateDirectory)/xt804_cpu_task_sw$(ObjectSuffix) -MF$(IntermediateDirectory)/xt804_cpu_task_sw$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/xt804_cpu_task_sw$(ObjectSuffix) $(IncludeAPath) $(IncludePackagePath)
Lst/xt804_cpu_task_sw$(PreprocessSuffix): ../Libraries/component/FreeRTOS/portable/xt804/cpu_task_sw.S
	$(CC) $(CFLAGS)$(IncludeAPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/xt804_cpu_task_sw$(PreprocessSuffix) ../Libraries/component/FreeRTOS/portable/xt804/cpu_task_sw.S

$(IntermediateDirectory)/xt804_port$(ObjectSuffix): ../Libraries/component/FreeRTOS/portable/xt804/port.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/portable/xt804/port.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/xt804_port$(ObjectSuffix) -MF$(IntermediateDirectory)/xt804_port$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/xt804_port$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/xt804_port$(PreprocessSuffix): ../Libraries/component/FreeRTOS/portable/xt804/port.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/xt804_port$(PreprocessSuffix) ../Libraries/component/FreeRTOS/portable/xt804/port.c

$(IntermediateDirectory)/MemMang_heap_5$(ObjectSuffix): ../Libraries/component/FreeRTOS/portable/MemMang/heap_5.c  
	$(CC) $(SourceSwitch) ../Libraries/component/FreeRTOS/portable/MemMang/heap_5.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/MemMang_heap_5$(ObjectSuffix) -MF$(IntermediateDirectory)/MemMang_heap_5$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/MemMang_heap_5$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/MemMang_heap_5$(PreprocessSuffix): ../Libraries/component/FreeRTOS/portable/MemMang/heap_5.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/MemMang_heap_5$(PreprocessSuffix) ../Libraries/component/FreeRTOS/portable/MemMang/heap_5.c

$(IntermediateDirectory)/lv_demo_widgets_lv_demo_widgets$(ObjectSuffix): ../lgvlV8/user/src/lv_demo_widgets/lv_demo_widgets.c  
	$(CC) $(SourceSwitch) ../lgvlV8/user/src/lv_demo_widgets/lv_demo_widgets.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/lv_demo_widgets_lv_demo_widgets$(ObjectSuffix) -MF$(IntermediateDirectory)/lv_demo_widgets_lv_demo_widgets$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/lv_demo_widgets_lv_demo_widgets$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/lv_demo_widgets_lv_demo_widgets$(PreprocessSuffix): ../lgvlV8/user/src/lv_demo_widgets/lv_demo_widgets.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/lv_demo_widgets_lv_demo_widgets$(PreprocessSuffix) ../lgvlV8/user/src/lv_demo_widgets/lv_demo_widgets.c

$(IntermediateDirectory)/flex_lv_flex$(ObjectSuffix): ../lgvlV8/src/extra/layouts/flex/lv_flex.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/layouts/flex/lv_flex.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/flex_lv_flex$(ObjectSuffix) -MF$(IntermediateDirectory)/flex_lv_flex$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/flex_lv_flex$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/flex_lv_flex$(PreprocessSuffix): ../lgvlV8/src/extra/layouts/flex/lv_flex.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/flex_lv_flex$(PreprocessSuffix) ../lgvlV8/src/extra/layouts/flex/lv_flex.c

$(IntermediateDirectory)/grid_lv_grid$(ObjectSuffix): ../lgvlV8/src/extra/layouts/grid/lv_grid.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/layouts/grid/lv_grid.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/grid_lv_grid$(ObjectSuffix) -MF$(IntermediateDirectory)/grid_lv_grid$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/grid_lv_grid$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/grid_lv_grid$(PreprocessSuffix): ../lgvlV8/src/extra/layouts/grid/lv_grid.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/grid_lv_grid$(PreprocessSuffix) ../lgvlV8/src/extra/layouts/grid/lv_grid.c

$(IntermediateDirectory)/basic_lv_theme_basic$(ObjectSuffix): ../lgvlV8/src/extra/themes/basic/lv_theme_basic.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/themes/basic/lv_theme_basic.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/basic_lv_theme_basic$(ObjectSuffix) -MF$(IntermediateDirectory)/basic_lv_theme_basic$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/basic_lv_theme_basic$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/basic_lv_theme_basic$(PreprocessSuffix): ../lgvlV8/src/extra/themes/basic/lv_theme_basic.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/basic_lv_theme_basic$(PreprocessSuffix) ../lgvlV8/src/extra/themes/basic/lv_theme_basic.c

$(IntermediateDirectory)/default_lv_theme_default$(ObjectSuffix): ../lgvlV8/src/extra/themes/default/lv_theme_default.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/themes/default/lv_theme_default.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/default_lv_theme_default$(ObjectSuffix) -MF$(IntermediateDirectory)/default_lv_theme_default$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/default_lv_theme_default$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/default_lv_theme_default$(PreprocessSuffix): ../lgvlV8/src/extra/themes/default/lv_theme_default.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/default_lv_theme_default$(PreprocessSuffix) ../lgvlV8/src/extra/themes/default/lv_theme_default.c

$(IntermediateDirectory)/mono_lv_theme_mono$(ObjectSuffix): ../lgvlV8/src/extra/themes/mono/lv_theme_mono.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/themes/mono/lv_theme_mono.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/mono_lv_theme_mono$(ObjectSuffix) -MF$(IntermediateDirectory)/mono_lv_theme_mono$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/mono_lv_theme_mono$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/mono_lv_theme_mono$(PreprocessSuffix): ../lgvlV8/src/extra/themes/mono/lv_theme_mono.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/mono_lv_theme_mono$(PreprocessSuffix) ../lgvlV8/src/extra/themes/mono/lv_theme_mono.c

$(IntermediateDirectory)/animimg_lv_animimg$(ObjectSuffix): ../lgvlV8/src/extra/widgets/animimg/lv_animimg.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/animimg/lv_animimg.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/animimg_lv_animimg$(ObjectSuffix) -MF$(IntermediateDirectory)/animimg_lv_animimg$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/animimg_lv_animimg$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/animimg_lv_animimg$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/animimg/lv_animimg.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/animimg_lv_animimg$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/animimg/lv_animimg.c

$(IntermediateDirectory)/calendar_lv_calendar$(ObjectSuffix): ../lgvlV8/src/extra/widgets/calendar/lv_calendar.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/calendar/lv_calendar.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/calendar_lv_calendar$(ObjectSuffix) -MF$(IntermediateDirectory)/calendar_lv_calendar$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/calendar_lv_calendar$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/calendar_lv_calendar$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/calendar/lv_calendar.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/calendar_lv_calendar$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/calendar/lv_calendar.c

$(IntermediateDirectory)/calendar_lv_calendar_header_arrow$(ObjectSuffix): ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_arrow.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_arrow.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/calendar_lv_calendar_header_arrow$(ObjectSuffix) -MF$(IntermediateDirectory)/calendar_lv_calendar_header_arrow$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/calendar_lv_calendar_header_arrow$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/calendar_lv_calendar_header_arrow$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_arrow.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/calendar_lv_calendar_header_arrow$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_arrow.c

$(IntermediateDirectory)/calendar_lv_calendar_header_dropdown$(ObjectSuffix): ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_dropdown.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_dropdown.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/calendar_lv_calendar_header_dropdown$(ObjectSuffix) -MF$(IntermediateDirectory)/calendar_lv_calendar_header_dropdown$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/calendar_lv_calendar_header_dropdown$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/calendar_lv_calendar_header_dropdown$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_dropdown.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/calendar_lv_calendar_header_dropdown$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/calendar/lv_calendar_header_dropdown.c

$(IntermediateDirectory)/chart_lv_chart$(ObjectSuffix): ../lgvlV8/src/extra/widgets/chart/lv_chart.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/chart/lv_chart.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/chart_lv_chart$(ObjectSuffix) -MF$(IntermediateDirectory)/chart_lv_chart$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/chart_lv_chart$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/chart_lv_chart$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/chart/lv_chart.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/chart_lv_chart$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/chart/lv_chart.c

$(IntermediateDirectory)/colorwheel_lv_colorwheel$(ObjectSuffix): ../lgvlV8/src/extra/widgets/colorwheel/lv_colorwheel.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/colorwheel/lv_colorwheel.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/colorwheel_lv_colorwheel$(ObjectSuffix) -MF$(IntermediateDirectory)/colorwheel_lv_colorwheel$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/colorwheel_lv_colorwheel$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/colorwheel_lv_colorwheel$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/colorwheel/lv_colorwheel.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/colorwheel_lv_colorwheel$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/colorwheel/lv_colorwheel.c

$(IntermediateDirectory)/imgbtn_lv_imgbtn$(ObjectSuffix): ../lgvlV8/src/extra/widgets/imgbtn/lv_imgbtn.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/imgbtn/lv_imgbtn.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/imgbtn_lv_imgbtn$(ObjectSuffix) -MF$(IntermediateDirectory)/imgbtn_lv_imgbtn$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/imgbtn_lv_imgbtn$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/imgbtn_lv_imgbtn$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/imgbtn/lv_imgbtn.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/imgbtn_lv_imgbtn$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/imgbtn/lv_imgbtn.c

$(IntermediateDirectory)/keyboard_lv_keyboard$(ObjectSuffix): ../lgvlV8/src/extra/widgets/keyboard/lv_keyboard.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/keyboard/lv_keyboard.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/keyboard_lv_keyboard$(ObjectSuffix) -MF$(IntermediateDirectory)/keyboard_lv_keyboard$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/keyboard_lv_keyboard$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/keyboard_lv_keyboard$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/keyboard/lv_keyboard.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/keyboard_lv_keyboard$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/keyboard/lv_keyboard.c

$(IntermediateDirectory)/led_lv_led$(ObjectSuffix): ../lgvlV8/src/extra/widgets/led/lv_led.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/led/lv_led.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/led_lv_led$(ObjectSuffix) -MF$(IntermediateDirectory)/led_lv_led$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/led_lv_led$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/led_lv_led$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/led/lv_led.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/led_lv_led$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/led/lv_led.c

$(IntermediateDirectory)/list_lv_list$(ObjectSuffix): ../lgvlV8/src/extra/widgets/list/lv_list.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/list/lv_list.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/list_lv_list$(ObjectSuffix) -MF$(IntermediateDirectory)/list_lv_list$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/list_lv_list$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/list_lv_list$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/list/lv_list.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/list_lv_list$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/list/lv_list.c

$(IntermediateDirectory)/meter_lv_meter$(ObjectSuffix): ../lgvlV8/src/extra/widgets/meter/lv_meter.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/meter/lv_meter.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/meter_lv_meter$(ObjectSuffix) -MF$(IntermediateDirectory)/meter_lv_meter$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/meter_lv_meter$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/meter_lv_meter$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/meter/lv_meter.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/meter_lv_meter$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/meter/lv_meter.c

$(IntermediateDirectory)/msgbox_lv_msgbox$(ObjectSuffix): ../lgvlV8/src/extra/widgets/msgbox/lv_msgbox.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/msgbox/lv_msgbox.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/msgbox_lv_msgbox$(ObjectSuffix) -MF$(IntermediateDirectory)/msgbox_lv_msgbox$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/msgbox_lv_msgbox$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/msgbox_lv_msgbox$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/msgbox/lv_msgbox.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/msgbox_lv_msgbox$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/msgbox/lv_msgbox.c

$(IntermediateDirectory)/span_lv_span$(ObjectSuffix): ../lgvlV8/src/extra/widgets/span/lv_span.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/span/lv_span.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/span_lv_span$(ObjectSuffix) -MF$(IntermediateDirectory)/span_lv_span$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/span_lv_span$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/span_lv_span$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/span/lv_span.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/span_lv_span$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/span/lv_span.c

$(IntermediateDirectory)/spinbox_lv_spinbox$(ObjectSuffix): ../lgvlV8/src/extra/widgets/spinbox/lv_spinbox.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/spinbox/lv_spinbox.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/spinbox_lv_spinbox$(ObjectSuffix) -MF$(IntermediateDirectory)/spinbox_lv_spinbox$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/spinbox_lv_spinbox$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/spinbox_lv_spinbox$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/spinbox/lv_spinbox.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/spinbox_lv_spinbox$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/spinbox/lv_spinbox.c

$(IntermediateDirectory)/spinner_lv_spinner$(ObjectSuffix): ../lgvlV8/src/extra/widgets/spinner/lv_spinner.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/spinner/lv_spinner.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/spinner_lv_spinner$(ObjectSuffix) -MF$(IntermediateDirectory)/spinner_lv_spinner$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/spinner_lv_spinner$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/spinner_lv_spinner$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/spinner/lv_spinner.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/spinner_lv_spinner$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/spinner/lv_spinner.c

$(IntermediateDirectory)/tabview_lv_tabview$(ObjectSuffix): ../lgvlV8/src/extra/widgets/tabview/lv_tabview.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/tabview/lv_tabview.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/tabview_lv_tabview$(ObjectSuffix) -MF$(IntermediateDirectory)/tabview_lv_tabview$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/tabview_lv_tabview$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/tabview_lv_tabview$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/tabview/lv_tabview.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/tabview_lv_tabview$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/tabview/lv_tabview.c

$(IntermediateDirectory)/tileview_lv_tileview$(ObjectSuffix): ../lgvlV8/src/extra/widgets/tileview/lv_tileview.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/tileview/lv_tileview.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/tileview_lv_tileview$(ObjectSuffix) -MF$(IntermediateDirectory)/tileview_lv_tileview$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/tileview_lv_tileview$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/tileview_lv_tileview$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/tileview/lv_tileview.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/tileview_lv_tileview$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/tileview/lv_tileview.c

$(IntermediateDirectory)/win_lv_win$(ObjectSuffix): ../lgvlV8/src/extra/widgets/win/lv_win.c  
	$(CC) $(SourceSwitch) ../lgvlV8/src/extra/widgets/win/lv_win.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/win_lv_win$(ObjectSuffix) -MF$(IntermediateDirectory)/win_lv_win$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/win_lv_win$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/win_lv_win$(PreprocessSuffix): ../lgvlV8/src/extra/widgets/win/lv_win.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/win_lv_win$(PreprocessSuffix) ../lgvlV8/src/extra/widgets/win/lv_win.c

$(IntermediateDirectory)/assets_img_clothes$(ObjectSuffix): ../lgvlV8/user/src/lv_demo_widgets/assets/img_clothes.c  
	$(CC) $(SourceSwitch) ../lgvlV8/user/src/lv_demo_widgets/assets/img_clothes.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/assets_img_clothes$(ObjectSuffix) -MF$(IntermediateDirectory)/assets_img_clothes$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/assets_img_clothes$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/assets_img_clothes$(PreprocessSuffix): ../lgvlV8/user/src/lv_demo_widgets/assets/img_clothes.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/assets_img_clothes$(PreprocessSuffix) ../lgvlV8/user/src/lv_demo_widgets/assets/img_clothes.c

$(IntermediateDirectory)/assets_img_demo_widgets_avatar$(ObjectSuffix): ../lgvlV8/user/src/lv_demo_widgets/assets/img_demo_widgets_avatar.c  
	$(CC) $(SourceSwitch) ../lgvlV8/user/src/lv_demo_widgets/assets/img_demo_widgets_avatar.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/assets_img_demo_widgets_avatar$(ObjectSuffix) -MF$(IntermediateDirectory)/assets_img_demo_widgets_avatar$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/assets_img_demo_widgets_avatar$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/assets_img_demo_widgets_avatar$(PreprocessSuffix): ../lgvlV8/user/src/lv_demo_widgets/assets/img_demo_widgets_avatar.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/assets_img_demo_widgets_avatar$(PreprocessSuffix) ../lgvlV8/user/src/lv_demo_widgets/assets/img_demo_widgets_avatar.c

$(IntermediateDirectory)/assets_img_lvgl_logo$(ObjectSuffix): ../lgvlV8/user/src/lv_demo_widgets/assets/img_lvgl_logo.c  
	$(CC) $(SourceSwitch) ../lgvlV8/user/src/lv_demo_widgets/assets/img_lvgl_logo.c $(CFLAGS) -MMD -MP -MT$(IntermediateDirectory)/assets_img_lvgl_logo$(ObjectSuffix) -MF$(IntermediateDirectory)/assets_img_lvgl_logo$(DependSuffix) $(ObjectSwitch)$(IntermediateDirectory)/assets_img_lvgl_logo$(ObjectSuffix) $(IncludeCPath) $(IncludePackagePath)
Lst/assets_img_lvgl_logo$(PreprocessSuffix): ../lgvlV8/user/src/lv_demo_widgets/assets/img_lvgl_logo.c
	$(CC) $(CFLAGS)$(IncludeCPath) $(PreprocessOnlySwitch) $(OutputSwitch) Lst/assets_img_lvgl_logo$(PreprocessSuffix) ../lgvlV8/user/src/lv_demo_widgets/assets/img_lvgl_logo.c


$(IntermediateDirectory)/__rt_entry$(ObjectSuffix): $(IntermediateDirectory)/__rt_entry$(DependSuffix)
	@$(AS) $(SourceSwitch) $(ProjectPath)/$(IntermediateDirectory)/__rt_entry.S $(ASFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/__rt_entry$(ObjectSuffix) $(IncludeAPath)
$(IntermediateDirectory)/__rt_entry$(DependSuffix):
	@$(CC) $(CFLAGS) $(IncludeAPath) -MG -MP -MT$(IntermediateDirectory)/__rt_entry$(ObjectSuffix) -MF$(IntermediateDirectory)/__rt_entry$(DependSuffix) -MM $(ProjectPath)/$(IntermediateDirectory)/__rt_entry.S

-include $(IntermediateDirectory)/*$(DependSuffix)
