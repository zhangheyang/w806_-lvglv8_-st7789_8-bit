# W806_LVGLv8_ST7789_8BIT

#### 介绍
联盛德W806 MCU芯片，价格便宜，带288K RAM，这么大内存，拿到手上第一感觉就是可以用来刷屏。
这个是移植LVLGv8+st7789 8位并口屏的DEMO。
DEMO很精糙，
https://whycan.com/viewtopic.php?pid=70144
想像中8BIT应该流畅很多，可能是水平不够，不知怎么优化。希望高手们指点。。
#### 接线说明：
屏的CS引脚 没接到板上，直接报了地；另外使用的屏无FMARK_PORT
可以从Project\src\st7789_parallel.h中修改相关定义。其它接线定义也请参考此头文件

  ' #define LCD_USE_FMARK_PORT 0
  ' #define LCD_USE_CS  0

