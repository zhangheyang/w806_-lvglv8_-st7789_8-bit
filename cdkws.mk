.PHONY: clean All Project_Title Project_Build Project_PostBuild

All: Project_Title Project_Build Project_PostBuild

Project_Title:
	@echo "----------Building project:[ W806_SDK - BuildSet ]----------"

Project_Build:
	@make -r -f W806_SDK.mk -j 8 -C  Project 

Project_PostBuild:
	@echo Executing Post Build commands ...
	@cd "Project" && export CDKPath="D:/develop/Hardware/CDK" CDK_VERSION="V2.10.1" ProjectPath="E:/project/Hardware/W806/W806_LVGLv8_ST7789_8BIT/w806_-lvglv8_-st7789_8-bit/Project/" && E:/project/Hardware/W806/W806_LVGLv8_ST7789_8BIT/w806_-lvglv8_-st7789_8-bit/Project/cdk_aft_build.sh;E:/project/Hardware/W806/W806_LVGLv8_ST7789_8BIT/w806_-lvglv8_-st7789_8-bit/Project/aft_build_project.sh 
	@echo Done


clean:
	@echo "----------Cleaning project:[ W806_SDK - BuildSet ]----------"

